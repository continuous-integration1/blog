-- migrate:up


create table posts (
  id bigserial,
  title text,
  text text
);


insert into posts (title,text) values ('Title', 'Post text');
insert into posts (title,text) values ('Title 2', 'Post text 2');


-- migrate:down

