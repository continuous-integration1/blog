import falcon

docs = [
    {u'title': u'Title2', u'text': u'Post text 2',
     u'tag': u'TestTag2', u'author': u'Author'},
    {u'title': u'Title', u'text': u'Post text',
     u'tag': u'TestTag', u'author': u'Author'},
]


class Resource(object):

    def on_get(self, req, resp):
        qs = falcon.uri.parse_query_string(req.query_string)
        if 'tag' in qs:
            tag = qs['tag']
            for doc in docs:
                if doc[u'tag'] == tag:
                    break
        else:
            doc = docs[-1]

        resp.media = doc
        resp.content_type = falcon.MEDIA_JSON
        resp.status = falcon.HTTP_200


class Tag(object):

    def on_get(self, req, resp):
        tags = []
        for doc in docs:
            tags.append(doc[u'tag'])

        resp.media = tags
        resp.content_type = falcon.MEDIA_JSON
        resp.status = falcon.HTTP_200
