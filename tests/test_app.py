import falcon
from falcon import testing
import pytest

from blog.app import api


@pytest.fixture
def client():
    return testing.TestClient(api)


def test_blog_post(client):
    doc = {u'title': u'Title', u'text': u'Post text',
           u'tag': u'TestTag', u'author': u'Author'}

    response = client.simulate_get('/post')

    assert response.json == doc
    assert response.status == falcon.HTTP_OK


def test_blog_post_search(client):

    doc = {u'title': u'Title2', u'text': u'Post text 2',
           u'tag': u'TestTag2', u'author': u'Author'}

    response = client.simulate_get('/post?tag=TestTag2')

    assert response.json == doc
    assert response.status == falcon.HTTP_OK


def test_blog_tag_list(client):
    tags = [u'TestTag2', u'TestTag']

    response = client.simulate_get('/tag')

    assert response.json == tags
    assert response.status == falcon.HTTP_OK
