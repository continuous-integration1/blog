
## How to install environment

```
pip3 install virualenv
virtualenv .venv
source .venv/bin/activate
pip3 install -r requirements.txt
```

## How to run app
```
gunicorn --reload blog.app
```
Endpoint works at http://localhost:8000/post

## How to run tests

```
pytest tests/
```